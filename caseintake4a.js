'use strict';
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
var mocha = require('mocha');
const args = process.argv[3];


module.exports = {
        
NewFive: function(driver){


it('should go to legalmatch.com/home/caseIntake4a.do', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

  });

}),

  it('should Input a One Line Summary of your Case ', function() {
  //--you can use id in choosing most common issues--//
  //--chooses the Adoption common issues in family category--//
  var summary = driver.wait(until.elementLocated(By.id('summary'))).sendKeys('Test One');

  summary.then(function(webElement){

    console.log('Element exists on summary');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on summary!');

    }

  });

}),
  it('should Describe your Case in Detail ', function() {
  //--you can use id in choosing most common issues--//
  //--chooses the Adoption common issues in family category--//
  var description = driver.wait(until.elementLocated(By.id('description'))).sendKeys('Hi Hello World Testing QA');

  description.then(function(webElement){

    console.log('Element exists on description');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on description!');

    }

  });

}),

 it('should click the next button', function() {

driver.wait(until.elementLocated(By.className('next'))).click();


  });

  }

}