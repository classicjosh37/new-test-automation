'use strict';
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
var mocha = require('mocha');
const args = process.argv[3];


module.exports = {
        
NewThree: function(driver){


it('should go to legalmatch.com/home/caseIntake.do', function() {

   driver.wait(function() {

  return driver.executeScript('return document.readyState').then(function(readyState) {

  return readyState === 'complete';

    });

 });

 }),

  it('should choose Most Common Issues ', function() {
  //--you can use id in choosing most common issues--//
  //--chooses the Adoption common issues in family category--//
  var issues = driver.wait(until.elementLocated(By.className('cat0'))).click();

  issues.then(function(webElement){

    console.log('Element exists on Common Issues');

  },function(err){

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on Common Issues!');

    }

  });

}),

 it('should click the next button', function() {

driver.wait(until.elementLocated(By.className('next'))).click();


});

}

}