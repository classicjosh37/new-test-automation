'use strict'
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
var mocha = require('mocha');
const args = process.argv[3];




module.exports = {

NewOne: function (driver) {
  //--opens a new chrome browser--//
  it('should go to legalmatch.com', function() {
    //--opens a browser with the legalmatch URL--//
    driver.get('https://'+args+'.legalmatch.com');
    //--Wait for the websites title to show before going to the next action--//
    driver.wait(function() {

    return driver.executeScript('return document.readyState').then(function(readyState) {

    return readyState === 'complete';

  });

});

  }),

  //it('should check the page source of the website', function() {
    //--print the Page Source of the website--//
   // var source = driver.getPageSource('https://'+args+'.legalmatch.com');
   // source.then(function (src) {
  //  console.log(source);
 // });
//  }),

  //it('should check the URL of the website', function() {
    //--checks the current URL of the website and compare it with the expected website--//
  //  var URL = driver.getCurrentUrl();

  //  var expectedUrl = 'https://'+args+'.legalmatch.com';

   // URL.then(function (URL) {

    //--print the current URL to console--//

  //  console.log('Website URL: ' +URL);

 // });

//  }),

  it('should choose a category ', function() {

  //--checks the toggle button element if it exist in the page source--//
  var toggle = driver.wait(until.elementLocated(By.className('case-intake-form__dropdown dropdown'))).click();

   toggle.then(function(webElement){

    console.log('Element exists on toggle button');

  },function(err){

    //--check if the console shows the text 'no such element' then it prints in console an alert text--//

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element does not exists on toggle button');

    }

  });

   //--checks the family element if it exist in the category--//
  var category = driver.wait(until.elementLocated(By.xpath('//*[@id="case-intake-form"]/div[1]/div[1]/div/div[1]'))).click();

  category.then(function(webElement){

    console.log('Element exists on category');

  },function(err){

     //--check if the console shows the text 'no such element' then it prints in console an alert text--//

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on category!');


    }

  });

}),
 it('should input a valid location', function() {

  //--checks the location text element if it exist--//

  var location = driver.wait(until.elementLocated(By.className('case-intake-form__field-item case-intake-form__input js-case-intake-location-input location-input'))).sendKeys('00001');

  location.then(function(webElement){

    console.log('Element exists on location');

  },function(err){

    //--check if the console shows the text 'no such element' then it prints in console an alert text--//

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on location!');

    }

  });

}),
  it('should click submit button', function() {

  //--checks the submit button element if it exist--//

  var submit = driver.findElement(By.className('case-intake-form__field-item case-intake-form__submit')).click();

  submit.then(function(webElement){

    console.log('Element exists on submit');

  },function(err){

    //--check if the console shows the text 'no such element' then it prints in console an alert text--//

    if(err.state && err.state === 'no such element') {

    }else{

      console.log('Alert! Element not found on submit!');

    }

  });

});

}


}